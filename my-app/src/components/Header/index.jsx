import { Component } from "react";
import "./header.scss";
import Button from "../Button";
import Modal from "../Modal";
import { modalFirst, modalSecond } from "../Modal/modals-conent";

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalTitle: '',
      modalText: '', 
      modalActions: [],
      modalCloseBtn: true, 
      modalIsOpen: false
    }
    this.modalChange = this.modalChange.bind(this)
    this.modalChangeState = this.modalChangeState.bind(this)
  }
  modalChange(title, text, actions, closeBtn){
    this.setState((state)=>({
      modalTitle: title,
      modalText: text, 
      modalActions: actions,
      modalCloseBtn: closeBtn, 
    }));
    this.modalChangeState();
  }
  modalChangeState(){
    this.setState((state)=>({
      modalIsOpen: !state.modalIsOpen
    }));
  }
  render() {
    return (
      <section className="main-header">
        <Button
          backgroundColor={"#ff3838"}
          text={"Open first window"}
          onButtonClick={this.modalChange}
          {...modalFirst}
        />
        <Button
          backgroundColor={"#72d846"}
          text={"Open second window"}
          onButtonClick={this.modalChange}
          {...modalSecond}

        />
        <Modal
          title={this.state.modalTitle}
          text={this.state.modalText}
          closeButton={this.state.modalCloseBtn}
          actions={this.state.modalActions}
          modalIsOpen={this.state.modalIsOpen}
          onCloseClick={this.modalChangeState}
        />
      </section>
    );
  }
}

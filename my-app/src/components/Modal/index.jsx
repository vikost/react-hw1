import { Component } from "react";
import "./modal.scss";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }

  closeModal(event) {
    if(!event.target.closest('.modal')||event.target.closest('.close-btn')){
      this.props.onCloseClick();
    }
  }

  render() {
    return (
      <div
        className={`${
          this.props.modalIsOpen ? "modal-wrap--active" : ""
        } modal-wrap`}
        onClick={this.closeModal.bind(this)}
      >
        <div className="modal">
          <header className="modal-header">
            <p className="modal-title">{this.props.title}</p>
            {this.props.closeButton ? (
              <button className="close-btn">
                <span></span>
                <span></span>
              </button>
            ) : null}
          </header>
          <section className="modal-content">
            <p className="modal-text">{this.props.text}</p>
          </section>
          <footer className="modal-footer">{this.props.actions}</footer>
        </div>
      </div>
    );
  }
}

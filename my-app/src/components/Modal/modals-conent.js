import Button from "../Button";

const modalFirst = {
    modalTitle: "Do you want delete this file?",
    modalText:  "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters",
    modalActions: [
      <Button key={1} backgroundColor={"rgb(170, 0, 0)"} text={"Ok"} onButtonClick={()=>true} />,
      <Button key={2} backgroundColor={"rgb(170, 0, 0)"} text={"Cancel"} onButtonClick={()=>true} />,
    ], 
    closeButton: true
  }
const modalSecond = {
    modalTitle: "Do you like this page?",
    modalText:  "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.",
    modalActions: [
      <Button key={1} backgroundColor={"rgb(170, 0, 0)"} text={"Yes"} onButtonClick={()=>true} />,
    ],
    closeButton: false
  }

export {modalFirst, modalSecond}
import { Component } from "react";
import "./button.scss";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <button
        style={{ backgroundColor: this.props.backgroundColor }}
        className="page-button"
        onClick={() => {
          this.props.onButtonClick(
            this.props.modalTitle,
            this.props.modalText,
            this.props.modalActions,
            this.props.closeButton
          );
        }}
      >
        {this.props.text}
      </button>
    );
  }
}
